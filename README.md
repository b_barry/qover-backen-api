  Quover Backend API based on Express js.

## Quick Start

  Install dependencies:

```bash
$ npm install
```

  Create a copy of .env.example, rename it '.env'


  Start the server ( build version):

```bash
$ npm start
```

  Start the server in dev mode:

```bash
$ npm run dev
```

  To build the source :

```bash
$ npm run build
```

  Start the lint :

```bash
$ npm run lint
```

## Docs

//TODO
We develop at *src/ folder* and compile all files with *babel* to *build/*.
*Don't update the build files because they would be overrided by the babel build.*

Create a folder/module for each related endpoint of the API with :
* a controller
* a model
* a index file
* a validation input: we use two npm package express-validation and joi to perform the input validation.
  Please  visit the repository of these 2
  package
* utils file if required

### Issues

If you discover a issue, please create a issue card in the Trello.


## Tests

  To run the test suite, first install the dependencies, then run `npm test`

  Create a copy of .env.example, rename it '.env.test'

```bash
$ npm install
$ npm test
```



## License

MIT
