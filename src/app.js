import express from "express";
import http from "http";
import path from "path";
import logger from "morgan";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import boom from "express-boom";
import cors from "cors";
import helmet from "helmet";
import config from "./config";



const app = express();
const port = process.env.PORT || '1337';
const connectionString = config.MONGO_CONNECTION_STRING;
const env = process.env.mode ? process.env.mode.trim() : 'dev';
let server;

app.set('port', port);
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'jade');
app.set('env', env);
app.use(boom());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, '../public')));
app.use(express.static(path.join(__dirname, '../public')));

app.use(helmet());
const corsOptions = {
  origin: config.CORS_WHITELIST,
  credentials: true
};
app.use(cors(corsOptions));
app.options('*', cors(corsOptions));

/**
 * Modules Handling
 */



function connect() {
  return mongoose.connect(connectionString).connection;
}

function listen() {
  if (app.get('env') === 'test') {
    return;
  }

  server = http.createServer(app);
  server.listen(port, ()=> {
    console.log('Express app started on port ' + port);
  });
}

connect()
  .on('error', console.log)
  .on('disconnected', console.log)
  .once('open', listen);

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

export default app;
