import dotenv from 'dotenv';

/* Dotenv loading */
if (process.env.mode && process.env.mode.trim() === 'test') {
  dotenv.load({path: '.env.test', silent: true});
} else {
  dotenv.load({silent: true});
}
const config = {};

config.port = process.env.PORT;
config.MONGO_CONNECTION_STRING = process.env.MONGO_CONNECTION_STRING.trim();
config.TOKEN_SECRET = process.env.TOKEN_SECRET.trim() || 'A hard to guess string';
config.CORS_WHITELIST = process.env.CORS_WHITELIST.trim().split(';');



export default config;
